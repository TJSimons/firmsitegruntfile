module.exports = function(grunt){
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        getUserId: function() {
            var path = (process.platform == 'win32') ? process.env['USERPROFILE'] : process.env['HOME'];
            return path.match(/u\d{6}/i);
        },
        banner: function () {
            // date-utils syntax
            var d = new Date().toString().slice(0, 15);
            var banner =  '/*\n';
                banner += '\tLast edited by: ' + this.getUserId() + ' on: ' + d + '\n';
                banner += '\tOriginal Author: <%= pkg.author %>';
                banner += '\n*/';

            return banner;
        },
        htmlhint : {
            options : {
                htmlhintrc: '.htmlhintrc'
            },
            src: ['design/templates/default.html']
        },
        sass : {
            build : {
                options : {
                    require: ['singularitygs'],
                    compass: true,
                    sourcemap: true,
                    style: 'compact',
                    quiet: true,
                    banner: '<%= banner() %>'
                },
                files: {
                    'design/css/site.css': 'design/css/site.scss'
                }
            }
        },
        csslint : {
            lax : {
                options : {
                    csslintrc : ".csslintrc"
                },
                src : "design/css/site.css"
            }
        },
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'design/images',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: 'design/images'
                }]
            }
        },
        jshint : {
            beforeconcat: ['design/init.js'],
            afterconcat: ['design/init.min.js']
        },
        uglify: {
            options: {
                mangle: false
            },
            prod: {
                files: {
                    'design/scripts/init.min.js': ['design/scripts/flScripts-1.0.1.js', 'design/scripts/init.js']
                },
                options: {
                    sourceMap: 'design/scripts/source-map.js'
                }
            }
        },
        watch: {
            html: {
                files: ['design/templates/*.html'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: ['design/scripts/init.js'],
                tasks: ['uglify'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: ['design/css/site.scss', 'design/css/import/*.scss'],
                tasks: ['sass'],
                
                options: {
                    livereload: true
                }
            }
        }
 
    });
 
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('test', ['htmlhint', 'uglify', 'jshint', 'csslint']);
    grunt.registerTask('build', ['uglify', 'sass', 'imagemin']);
 
};